package PO_pattern;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;

public class AlphaBankPage extends AbstractPage {

    @Name("Vacancy")
    @FindBy(linkText = "Вакансии")
    Button vacancyButton;

    @Name("VacancyTextBlock")
    @FindBy(xpath = "//*[@id=\"content\"]/div[2]/div/div[1]")
    public Button vacancyTextBlock;

    public AlphaBankPage(WebDriver driver) {
        super(driver);
    }
    
    public void vacancy(String request) {
       vacancyButton.sendKeys(request);
       vacancyButton.sendKeys(Keys.ENTER);
    }

}
