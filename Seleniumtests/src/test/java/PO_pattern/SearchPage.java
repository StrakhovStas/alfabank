package PO_pattern;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.htmlelements.annotations.Name;
import ru.yandex.qatools.htmlelements.element.Button;
import ru.yandex.qatools.htmlelements.element.TextInput;

public class SearchPage extends AbstractPage {

    @Name("Search request input")
    @FindBy(xpath = "//*[@id=\"text\"]")
    TextInput requestInput;

    @Name("Search button")
    @FindBy(xpath = "/html/body/div[1]/div[2]/div[3]/div/div[2]/div/ftgr/div/div[2]/div/div[2]/div/form/div[2]/button]")
    Button searchButton;

    public SearchPage() {
        super();
    }

    public void search(String request) {
        requestInput.sendKeys(request);
        requestInput.sendKeys(Keys.ENTER);
    }

    public SearchPage(WebDriver driver) {
        super(driver);
    }

}