import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.common.TestBase;

import java.util.List;

import static junit.framework.TestCase.fail;


public class FirstTest extends TestBase {

    @Test
    public void firstTestCase() throws Exception {
        goToYandexPage();
        switchToYandexMarketTab();
        sweetchToElectronicks();
        clickOnText("���������");
        selectPhoneModel("Samsung");
        setPriceFrom(40000);
        saveNameFromFirstItem();
        switchToFirstElementPageViaLink();
        verifyElementTitles();
        sweetchToElectronicks();
        clickOnText("�������� � Bluetooth-���������");
        selectPhoneModel("Beats");
        setPriceFrom(17000);
        setPriceUnder(25000);
        saveNameFromFirstItem();
        switchToFirstElementPageViaLink();
        verifyElementTitles();
        driver.quit();
    }

    private void verifyElementTitles() {
        String verifiedElement = driver.findElement(By.xpath("//h1")).getText();
        Assert.assertEquals("�������� �� ���������", verifiedElement, elementName1);
    }

    private void setElementName1(String elementName1) {
        this.elementName1 = elementName1;
    }

    private String elementName1;

    private String saveNameFromFirstItem() {
        String elementName = driver.findElement(By.xpath("(.//*[@class='n-snippet-cell2__title']//a)[1]")).getText();
        setElementName1(elementName);
        return elementName;
    }

    private String returnLinkFromFirstItem() {
        return driver.findElement(By.xpath("(.//*[@class='n-snippet-cell2__title']//a)[1]")).getAttribute("href");

    }

    private void switchToFirstElementPageViaLink() {
        driver.get(returnLinkFromFirstItem());
    }

    private void selectPhoneModel(String phoneModel) {
        String checkBoxselected = ("//*[@type='checkbox']/..//*[contains(text(),'" + phoneModel + "')]");
        driver.findElement(By.xpath(checkBoxselected)).click();
    }

    private void setPriceFrom(int price) {
        driver.findElement(By.xpath("//*[@id=\"glpricefrom\"]")).sendKeys(String.valueOf(price));
    }
    
    private void setPriceUnder(int price) {
        driver.findElement(By.xpath("//*[@id=\"glpriceto\"]")).sendKeys(String.valueOf(price));
    }

    protected void clickOnText(String s) {
        driver.findElement(By.linkText(s)).click();
    }

    private void switchToYandexMarketTab() {
        WebElement marketTab = By.xpath("//*[contains(text(),'������')][@data-id = 'market']").findElement(driver);
        marketTab.click();
    }

    private void sweetchToElectronicks() {
        driver.findElement(By.linkText("�����������")).click();
    }
}