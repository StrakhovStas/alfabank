import java.io.*;
import java.nio.file.*;
import java.util.Arrays;
import java.util.stream.Stream;

public class Listofnumber {
public static void main(String[] args) throws Exception {
	try
	{
	    Path path = Paths.get("test1.txt");
	    int[] numbers = Files.lines(path)
	            .flatMap(e -> Stream.of(e.split(",")))
	            .mapToInt(Integer::parseInt)
	            .toArray();
	        for (int i = 0; i < numbers.length; i++) {
	            int min = numbers[i];
	            int min_i = i; 
	            for (int j = i+1; j < numbers.length; j++) {
	                if (numbers[j] < min) {
	                    min = numbers[j];
	                    min_i = j;
	                }
	            }
	            if (i != min_i) {
	                int tmp = numbers[i];
	                numbers[i] = numbers[min_i];
	                numbers[min_i] = tmp;
	            }
	        }       
			System.out.println(Arrays.toString(numbers));
			for (int i = 0; i < numbers.length; i++) {
				int max = numbers[i];
				int max_i = i; 
				for (int j = i+1; j < numbers.length; j++) {
					if (numbers[j] > max) {
						max = numbers[j];
						max_i = j;
					}
				}
				if (i != max_i) {
					int tmp = numbers[i];
					numbers[i] = numbers[max_i];
					numbers[max_i] = tmp;
				}
			}       
			System.out.println(Arrays.toString(numbers));
	}
	catch (IOException e)
	{
	    e.printStackTrace();
	}
    }
}